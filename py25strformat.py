#!/usr/bin/env python2.5
__doc__ = 'backport of str.format and unicode.format to Python 2.5'

import sys
if sys.version_info >= (2, 6):
    raise ImportError, "format and __format__ methods already builtin"
elif sys.version_info < (2, 5):
    raise Warning, "py25format not tested on Python < 2.5"

import re
import StringIO

next_brace_regex = re.compile(r'''(?P<literal>[^{}]*) # literal text 
                                  (?P<brace>[{}])?    # a single brace
                               ''', re.VERBOSE)
def _parse(string):
    brace_depth = 0

    get_next_brace = re.finditer(next_brace_regex, string)
    for m in get_next_brace:

        match = m.group()
        literals = m.group('literal')
        brace = m.group('brace')

        end = m.end()
        char, next_char = m.string[end-1:end], m.string[end:end + 1]

        ## an escaped brace '{{' or '}}'
        if brace == next_char and not brace_depth: 
            yield False, match # return literal '{' or '}'
            get_next_brace.next() # skip the extra brace
            continue

        ## starting, closing, or inside a format field
        if brace:
            if char == '}':
                brace_depth -= 1

                if brace_depth == 0: # closing format string

                    # handle format field here
                    yield True, format_str + match
                    format_str = ''
                    continue
                elif brace_depth < 0:
                    raise ValueError, "Unmatched '}' brace"

            elif char == '{':
                brace_depth += 1

                if brace_depth == 1: # starting format string
                    yield False, literals
                    format_str = brace # new format string '{'
                    continue
                elif brace_depth > 2:
                    raise ValueError, "Max string recursion exceeded"
            
            # add literal or brace to format string
            format_str += match

        ## literals or end of string
        else:
            if end == len(string) and brace_depth != 0:
                raise ValueError, "Unmatched or unescaped '}' or '{' brace in format string"
            yield False, literals

def _get_first_part(field_spec):
    # autonumbering
    if field_spec == '':
        autonumber = 0
        return autonumber, ''

    # the int or string before the first '.' or '['
    arg = re.match(r'\d+', field_spec)
    if arg:
        return int(arg.group()), field_spec[arg.end():]
    
    kwd = re.match(r'[- _\w]+', field_spec)
    if kwd:
        return kwd.group(), field_spec[kwd.end():]

def _get_value(field, args, kwds):
    'gets the first value for the first field'
    
    if type(field) == int:
        if abs(field) < len(args):
            value = args[field]
        else:
            raise IndexError, "Index out of range"
    elif type(field) == str:
        if field in kwds.keys():
            value = kwds[field]
        else:
            raise KeyError, "Invalid Field"    
    return value
    
def _components(field_spec):
    # [subfield] or .subfield
    #print 'in components with field_spec',  repr(field_spec)
    for m in re.finditer(r'\.[- _\w]+|[\[\{][- _\w]+[\]\}]', field_spec):
        subfield = m.group().strip('.[]{}')
        if re.match(r'\d+', subfield):
            yield int(subfield)
        else:
            yield subfield
        field_spec = field_spec[m.end():]
            
    if '.' in field_spec:
        raise ValueError, "Unknown '.' subfield"
    if '[' in field_spec:
        raise ValueError, "Unknown '[' subfield"

def _resolve_subfield(value, subfield):
    #print 'resolving subfields:', repr(value), repr(subfield)
    if type(subfield) == str and hasattr(value, subfield):
        return getattr(value, subfield)
    elif hasattr(value, '__getitem__'):
        return value.__getitem__(subfield)
    elif hasattr(value, '__format__'):
        return value.__format__(subfield)
    else:
        return value

# from PEP 3101 - Formatter Methods
def _vformat(format_string, args, kwargs):
     
    # Output buffer and set of used args
    buffer = StringIO.StringIO()
    used_args = set()
    
    # Tokens are either format fields or literal strings
    for is_format_field, token in _parse(format_string):
        if is_format_field:
            assert token[0] == '{' and token[-1] == '}'
            token = token[1:-1]

            #print 'token', repr(token)

            # expand computed format specifiers like {0:{1}}
            for m in re.finditer('{.+?}', token):
                subformat = m.group().strip('{}')
                if re.match(r'\d+', subformat):
                    subformat = int(subformat)
                new = _get_value(subformat, args, kwargs)
                token = token.replace(m.group(), unicode(new))

            # Split the token into field value and format spec
            field_spec, _, format_spec = token.partition(":")

            # Check for explicit type conversion
            #conversion, _, field_spec = field_spec.rpartition("!") # for no !
            if '!' in field_spec:
                field_spec, _, conversion = field_spec.rpartition("!")
                if len(conversion) != 1 or conversion not in ('r', 's'):
                    raise ValueError, "conversion must be 'r' or 's'"
            else:
                conversion = 's'

            #print 'field spec, conversion', repr(field_spec), repr(conversion)

            # 'first_part' is the part before the first '.' or '['
            # Assume that 'get_first_part' returns either an int or
            # a string, depending on the syntax.

            first_part, field_spec = _get_first_part(field_spec)
            value = _get_value(first_part, args, kwargs)
            
            # Record the fact that we used this arg
            used_args.add(first_part)
              
            # For [subfield] or .subfield, 'components'
            # is an iterator over subfields, excluding
            # the first part.
            #print 'field_spec', repr(field_spec)
            for comp in _components(field_spec):
                value = _resolve_subfield(value, comp)
            
            if field_spec == '' and hasattr(value, '__format__'):
                value = value.__format__(format_spec)
            elif hasattr(value, '__str__'):
                value = value.__str__()
            elif hasattr(value, '__unicode__'):
                value = value.__unicode__()

            # Handle type conversion
            if conversion == 'r':
                value = repr(value)
            elif conversion == 's':
                value = str(value)

            # Call the global 'format' function
            value = _format_field(value, format_spec)

            # write out the converted value.
            buffer.write(value)            
        else:
            buffer.write(token)
        #_check_unused_args(used_args, args, kwargs)
    return buffer.getvalue()

format_field_regex = re.compile(r'''
(?P<align>[^{}][<>=\^]|[<>=\^])?    # align and fill
(?P<sign>[+\- ])?                   # + or - sign before numbers
(?P<prefix>\#)?                     # prefix binary, octal, or hex integers
(?P<zeropad>0)?                     # 
(?P<width>\d+)?                     # minimum field width
(?P<thousands>\,)?                  # use comma for thousands
(?P<precision>\.\d+)?               # max width for str or digits after decimal
(?P<type>[bcdeEfFgGnosxX\%])?       #
''', re.VERBOSE)

def _format_field(value, format_spec):
    'add __format__ method to builtin types'

    m = re.search(format_field_regex, format_spec)

    align = m.group('align')
    width = m.group('width')
    precision = m.group('precision')

    # TODO __format__ for numeric types
    # prefix = m.group('prefix')

    # if m.group('thousands'):
    #     pass
    # if m.group('zeropad'):
    #     pass

    # newtype = m.group('type')
    # if newtype:
    #     pass
    # else:
    #     value = str(value)

    if precision:
        if type(value) == int:
            raise TypeError, "Cannot format integer with precision"

        # drop decimal point '.' from beginning
        precision = int(precision[1:])

        if type(value) in (float, long, complex):
            # TODO digits after decimal point
            pass
        else:
            # max_length for non numbers
            max_length = precision

        if precision < len(value):
            value = value[:precision]

    if width:
        min_length = int(width)
        to_fill = min_length - len(value)

        if align and len(align) > 1:
            fill_char = align[0]
            align = align[1]
        elif align and len(align) == 1:
            fill_char = ' ' # align = align[0]
        else:
            fill_char = ' '
            align = '<'

        if to_fill > 0:
            if align == '<': # left
                value = value + (fill_char * to_fill)
            elif align == '>': # right
                value = (fill_char * to_fill) + value
            elif align == '^': # center
                both, right = divmod(to_fill, 2)
                left_space = fill_char * both
                right_space = fill_char * (both + right)
                value =  left_space + value + right_space
    if len(format_spec) > 0 and not m:
        raise ValueError, "Invalid format specification"
    return value

class Formattable(object):
    def __init__(self, stringlike):
        self.template = stringlike

    def __format__(self, format_spec):
        if type(self) != str and hasattr(self, '__str__'):
            return _format_field(self.__str__(), format_spec)
        else:
            return _format_field(self, format_spec)

    def format(self, *args, **kwds):
        return self.vformat(args, kwds)

    def vformat(self, args, kwds):
        if hasattr(self, 'template'): # format called later
            return _vformat(self.template, args, kwds)
        else: # format called immediatedly FormatString().format()
            return _vformat(self, args, kwds)

class FormatUnicode(Formattable, unicode): pass
class FormatString(Formattable, str): pass


def main():
    import unittest
    from test_str import StrTest
    suite = unittest.TestLoader().loadTestsFromTestCase(StrTest)
    unittest.TextTestRunner(verbosity=2).run(suite)

if __name__ == '__main__':
    main()
    #print FormatString("{0]}").format()
    #print FormatString("{0..foo}").format(0) # ValueError
    #print FormatString("{0!x}").format(3) # ValueError
    #print FormatString('{0[{1}]}').format('abcdefg', 4) # TypeError
    #print repr(FormatString("{0:-s}").format(''))
