#!/usr/bin/env python2.5

# Python-2.7/Lib/test/test_str.py
from py25strformat import FormatString, Formattable
import unittest

import struct
import sys
import string_tests


class StrTest(
    string_tests.CommonTest,
    string_tests.MixinStrUnicodeUserStringTest,
    string_tests.MixinStrUserStringTest,
    string_tests.MixinStrUnicodeTest,
    ):

    type2test = FormatString

    # We don't need to propagate to str
    def fixtype(self, obj):
        return obj

    def test_basic_creation(self):
        self.assertEqual(FormatString(''), '')
        self.assertEqual(FormatString(0), '0')
        self.assertEqual(FormatString(0L), '0')
        self.assertEqual(FormatString(()), '()')
        self.assertEqual(FormatString([]), '[]')
        self.assertEqual(FormatString({}), '{}')
        a = []
        a.append(a)
        self.assertEqual(FormatString(a), '[[...]]')
        a = {}
        a[0] = a
        self.assertEqual(FormatString(a), '{0: {...}}')

## ignoring % formatting 
#    def test_formatting(self):
#        string_tests.MixinStrUnicodeUserStringTest.test_formatting(self)
#        self.assertRaises(OverflowError, '%c'.__mod__, 0x1234)

    def test_conversion(self):
        # Make sure __str__() behaves properly
        class Foo0:
            def __unicode__(self):
                return u"foo"

        class Foo1:
            def __str__(self):
                return "foo"

        class Foo2(object):
            def __str__(self):
                return "foo"

        class Foo3(object):
            def __str__(self):
                return u"foo"

        class Foo4(unicode):
            def __str__(self):
                return u"foo"

        class Foo5(str):
            def __str__(self):
                return u"foo"

        class Foo6(str):
            def __str__(self):
                return "foos"

            def __unicode__(self):
                return u"foou"

        class Foo7(unicode):
            def __str__(self):
                return "foos"
            def __unicode__(self):
                return u"foou"

        class Foo8(str):
            def __new__(cls, content=""):
                return str.__new__(cls, 2*content)
            def __str__(self):
                return self

        class Foo9(str):
            def __str__(self):
                return "string"
            def __unicode__(self):
                return "not unicode"

        self.assertTrue(str(Foo0()).startswith("<")) # this is different from __unicode__
        self.assertEqual(str(Foo1()), "foo")
        self.assertEqual(str(Foo2()), "foo")
        self.assertEqual(str(Foo3()), "foo")
        self.assertEqual(str(Foo4("bar")), "foo")
        self.assertEqual(str(Foo5("bar")), "foo")
        self.assertEqual(str(Foo6("bar")), "foos")
        self.assertEqual(str(Foo7("bar")), "foos")
        self.assertEqual(str(Foo8("foo")), "foofoo")
        self.assertEqual(str(Foo9("foo")), "string")
        self.assertEqual(unicode(Foo9("foo")), u"not unicode")

    def test_expandtabs_overflows_gracefully(self):
        # This test only affects 32-bit platforms because expandtabs can only take
        # an int as the max value, not a 64-bit C long.  If expandtabs is changed
        # to take a 64-bit long, this test should apply to all platforms.
        if sys.maxint > (1 << 32) or struct.calcsize('P') != 4:
            return
        self.assertRaises(OverflowError, 't\tt\t'.expandtabs, sys.maxint)

    def test__format__(self):
        def test(value, format, expected):
            value = FormatString(value)

            # test both with and without the trailing 's'
            self.assertEqual(value.__format__(format), expected)
            self.assertEqual(value.__format__(format + 's'), expected)

        test('', '', '')
        test('abc', '', 'abc')
        test('abc', '.3', 'abc')
        test('ab', '.3', 'ab')
        test('abcdef', '.3', 'abc')
        test('abcdef', '.0', '')
        test('abc', '3.3', 'abc')
        test('abc', '2.3', 'abc')
        test('abc', '2.2', 'ab')
        test('abc', '3.2', 'ab ')
        test('result', 'x<0', 'result')
        test('result', 'x<5', 'result')
        test('result', 'x<6', 'result')
        test('result', 'x<7', 'resultx')
        test('result', 'x<8', 'resultxx')
        test('result', ' <7', 'result ')
        test('result', '<7', 'result ')
        test('result', '>7', ' result')
        test('result', '>8', '  result')
        test('result', '^8', ' result ')
        test('result', '^9', ' result  ')
        test('result', '^10', '  result  ')
        ## TODO additions to test other builtin types esp. numerics
        # (int, float, complex) go here

        test('a', '10000', 'a' + ' ' * 9999)
        test('', '10000', ' ' * 10000)
        test('', '10000000', ' ' * 10000000)

    def test_format(self):
        self.assertEqual(FormatString('').format(), '')
        self.assertEqual(FormatString('a').format(), 'a')
        self.assertEqual(FormatString('ab').format(), 'ab')
        self.assertEqual(FormatString('a{{').format(), 'a{')
        self.assertEqual(FormatString('a}}').format(), 'a}')
        self.assertEqual(FormatString('{{b').format(), '{b')
        self.assertEqual(FormatString('}}b').format(), '}b')
        self.assertEqual(FormatString('a{{b').format(), 'a{b')

        # examples from the PEP:
        import datetime
        self.assertEqual(FormatString("My name is {0}").format('Fred'), "My name is Fred")
        self.assertEqual(FormatString("My name is {0[name]}").format(dict(name='Fred')), "My name is Fred")
        self.assertEqual(FormatString("My name is {0} :-{{}}").format('Fred'),
                         "My name is Fred :-{}")

        d = datetime.date(2007, 8, 18)
        self.assertEqual(FormatString("The year is {0.year}").format(d), "The year is 2007")

        # classes we'll use for testing
        class C:
            def __init__(self, x=100):
                self._x = x
            def __format__(self, spec):
                return spec

        class D:
            def __init__(self, x):
                self.x = x
            def __format__(self, spec):
                return str(self.x)

        # class with __str__, but no __format__
        class E:
            def __init__(self, x):
                self.x = x
            def __str__(self):
                return 'E(' + self.x + ')'

        # class with __repr__, but no __format__ or __str__
        class F:
            def __init__(self, x):
                self.x = x
            def __repr__(self):
                return 'F(' + self.x + ')'

        # class with __format__ that forwards to string, for some format_spec's
        class G(Formattable):
            def __init__(self, x):
                self.x = x
            def __str__(self):
                return "string is " + self.x
            def __format__(self, format_spec):
                if format_spec == 'd':
                    return 'G(' + self.x + ')'
                return object.__format__(format_spec)

        # class that returns a bad type from __format__
        class H:
            def __format__(self, format_spec):
                return 1.0

        class I(datetime.date):
            def __format__(self, format_spec):
                return self.strftime(format_spec)

        class J(Formattable, int):
            def __format__(self, format_spec):
                return object.__format__(self * 2, format_spec)


        self.assertEqual(FormatString('').format(), '')
        self.assertEqual(FormatString('abc').format(), 'abc')
        self.assertEqual(FormatString('{0}').format('abc'), 'abc')
        self.assertEqual(FormatString('{0:}').format('abc'), 'abc')
        self.assertEqual(FormatString('X{0}').format('abc'), 'Xabc')
        self.assertEqual(FormatString('{0}X').format('abc'), 'abcX')
        self.assertEqual(FormatString('X{0}Y').format('abc'), 'XabcY')
        self.assertEqual(FormatString('{1}').format(1, 'abc'), 'abc')
        self.assertEqual(FormatString('X{1}').format(1, 'abc'), 'Xabc')
        self.assertEqual(FormatString('{1}X').format(1, 'abc'), 'abcX')
        self.assertEqual(FormatString('X{1}Y').format(1, 'abc'), 'XabcY')
        self.assertEqual(FormatString('{0}').format(-15), '-15')
        self.assertEqual(FormatString('{0}{1}').format(-15, 'abc'), '-15abc')
        self.assertEqual(FormatString('{0}X{1}').format(-15, 'abc'), '-15Xabc')
        self.assertEqual(FormatString('{{').format(), '{')
        self.assertEqual(FormatString('}}').format(), '}')
        self.assertEqual(FormatString('{{}}').format(), '{}')
        self.assertEqual(FormatString('{{x}}').format(), '{x}')
        self.assertEqual(FormatString('{{{0}}}').format(123), '{123}')
        self.assertEqual(FormatString('{{{{0}}}}').format(), '{{0}}')
        self.assertEqual(FormatString('}}{{').format(), '}{')
        self.assertEqual(FormatString('}}x{{').format(), '}x{')

        # weird field names
        self.assertEqual(FormatString("{0[foo-bar]}").format({'foo-bar':'baz'}), 'baz')
        self.assertEqual(FormatString("{0[foo bar]}").format({'foo bar':'baz'}), 'baz')
        self.assertEqual(FormatString("{0[ ]}").format({' ':3}), '3')

        self.assertEqual(FormatString('{foo._x}').format(foo=C(20)), '20')
        self.assertEqual(FormatString('{1}{0}').format(D(10), D(20)), '2010')
        self.assertEqual(FormatString('{0._x.x}').format(C(D('abc'))), 'abc')
        self.assertEqual(FormatString('{0[0]}').format(['abc', 'def']), 'abc')
        self.assertEqual(FormatString('{0[1]}').format(['abc', 'def']), 'def')
        self.assertEqual(FormatString('{0[1][0]}').format(['abc', ['def']]), 'def')
        self.assertEqual(FormatString('{0[1][0].x}').format(['abc', [D('def')]]), 'def')

        # strings
        self.assertEqual(FormatString('{0:.3s}').format('abc'), 'abc')
        self.assertEqual(FormatString('{0:.3s}').format('ab'), 'ab')
        self.assertEqual(FormatString('{0:.3s}').format('abcdef'), 'abc')
        self.assertEqual(FormatString('{0:.0s}').format('abcdef'), '')
        self.assertEqual(FormatString('{0:3.3s}').format('abc'), 'abc')
        self.assertEqual(FormatString('{0:2.3s}').format('abc'), 'abc')
        self.assertEqual(FormatString('{0:2.2s}').format('abc'), 'ab')
        self.assertEqual(FormatString('{0:3.2s}').format('abc'), 'ab ')
        self.assertEqual(FormatString('{0:x<0s}').format('result'), 'result')
        self.assertEqual(FormatString('{0:x<5s}').format('result'), 'result')
        self.assertEqual(FormatString('{0:x<6s}').format('result'), 'result')
        self.assertEqual(FormatString('{0:x<7s}').format('result'), 'resultx')
        self.assertEqual(FormatString('{0:x<8s}').format('result'), 'resultxx')
        self.assertEqual(FormatString('{0: <7s}').format('result'), 'result ')
        self.assertEqual(FormatString('{0:<7s}').format('result'), 'result ')
        self.assertEqual(FormatString('{0:>7s}').format('result'), ' result')
        self.assertEqual(FormatString('{0:>8s}').format('result'), '  result')
        self.assertEqual(FormatString('{0:^8s}').format('result'), ' result ')
        self.assertEqual(FormatString('{0:^9s}').format('result'), ' result  ')
        self.assertEqual(FormatString('{0:^10s}').format('result'), '  result  ')
        self.assertEqual(FormatString('{0:10000}').format('a'), 'a' + ' ' * 9999)
        self.assertEqual(FormatString('{0:10000}').format(''), ' ' * 10000)
        self.assertEqual(FormatString('{0:10000000}').format(''), ' ' * 10000000)

        # format specifiers for user defined type
        self.assertEqual(FormatString('{0:abc}').format(C()), 'abc')

        # !r and !s coersions
        self.assertEqual(FormatString('{0!s}').format('Hello'), 'Hello')
        self.assertEqual(FormatString('{0!s:}').format('Hello'), 'Hello')
        self.assertEqual(FormatString('{0!s:15}').format('Hello'), 'Hello          ')
        self.assertEqual(FormatString('{0!s:15s}').format('Hello'), 'Hello          ')
        self.assertEqual(FormatString('{0!r}').format('Hello'), "'Hello'")
        self.assertEqual(FormatString('{0!r:}').format('Hello'), "'Hello'")
        self.assertEqual(FormatString('{0!r}').format(F('Hello')), 'F(Hello)')

        # test fallback to object.__format__
        self.assertEqual(FormatString('{0}').format({}), '{}')
        self.assertEqual(FormatString('{0}').format([]), '[]')
        self.assertEqual(FormatString('{0}').format([1]), '[1]')
        self.assertEqual(FormatString('{0}').format(E('data')), 'E(data)')
        self.assertEqual(FormatString('{0:^10}').format(E('data')), ' E(data)  ')
        self.assertEqual(FormatString('{0:^10s}').format(E('data')), ' E(data)  ')
        self.assertEqual(FormatString('{0:d}').format(G('data')), 'G(data)')
# TODO        self.assertEqual(FormatString('{0:>15s}').format(G('data')), ' string is data')
# TODO        self.assertEqual(FormatString('{0!s}').format(G('data')), 'string is data')

        self.assertEqual(FormatString("{0:date: %Y-%m-%d}").format(I(year=2007,
                                                       month=8,
                                                       day=27)),
                         "date: 2007-08-27")

        # test deriving from a builtin type and overriding __format__
# TODO        self.assertEqual(FormatString("{0}").format(J(10)), "20")


        # string format specifiers
        self.assertEqual(FormatString('{0:}').format('a'), 'a')

        # computed format specifiers
        self.assertEqual(FormatString("{0:.{1}}").format('hello world', 5), 'hello')
        self.assertEqual(FormatString("{0:.{1}s}").format('hello world', 5), 'hello')
        self.assertEqual(FormatString("{0:.{precision}s}").format('hello world', precision=5), 'hello')
        self.assertEqual(FormatString("{0:{width}.{precision}s}").format('hello world', width=10, precision=5), 'hello     ')
        self.assertEqual(FormatString("{0:{width}.{precision}s}").format('hello world', width='10', precision='5'), 'hello     ')

        # test various errors
        self.assertRaises(ValueError, FormatString('{').format)
        self.assertRaises(ValueError, FormatString('}').format)
        self.assertRaises(ValueError, FormatString('a{').format)
        self.assertRaises(ValueError, FormatString('a}').format)
        self.assertRaises(ValueError, FormatString('{a').format)
        self.assertRaises(ValueError, FormatString('}a').format)
        self.assertRaises(IndexError, FormatString('{0}').format)
        self.assertRaises(IndexError, FormatString('{1}').format, 'abc')
        self.assertRaises(KeyError,   FormatString('{x}').format)
        self.assertRaises(ValueError, FormatString("}{").format)
        self.assertRaises(ValueError, FormatString("{").format)
        self.assertRaises(ValueError, FormatString("}").format)
        self.assertRaises(ValueError, FormatString("abc{0:{}").format)
        self.assertRaises(ValueError, FormatString("{0").format)
        self.assertRaises(IndexError, FormatString("{0.}").format)
        self.assertRaises(ValueError, FormatString("{0.}").format, 0)
        self.assertRaises(IndexError, FormatString("{0[}").format)
        self.assertRaises(ValueError, FormatString("{0[}").format, [])
# TODO        self.assertRaises(KeyError,   FormatString("{0]}").format)
        self.assertRaises(ValueError, FormatString("{0.[]}").format, 0)
# TODO        self.assertRaises(ValueError, FormatString("{0..foo}").format, 0)
        self.assertRaises(ValueError, FormatString("{0[0}").format, 0)
        self.assertRaises(ValueError, FormatString("{0[0:foo}").format, 0)
        self.assertRaises(KeyError,   FormatString("{c]}").format)
        self.assertRaises(ValueError, FormatString("{{ {{{0}}").format, 0)
        self.assertRaises(ValueError, FormatString("{0}}").format, 0)
        self.assertRaises(KeyError,   FormatString("{foo}").format, bar=3)
        self.assertRaises(ValueError, FormatString("{0!x}").format, 3)
        self.assertRaises(ValueError, FormatString("{0!}").format, 0)
        self.assertRaises(ValueError, FormatString("{0!rs}").format, 0)
        self.assertRaises(ValueError, FormatString("{!}").format)
        self.assertRaises(IndexError, FormatString("{:}").format)
        self.assertRaises(IndexError, FormatString("{:s}").format)
        self.assertRaises(IndexError, FormatString("{}").format)

        # issue 6089
# TODO        self.assertRaises(ValueError, FormatString("{0[0]x}").format, [None])
# TODO        self.assertRaises(ValueError, FormatString("{0[0](10)}").format, [None])

        # can't have a replacement on the field name portion
# TODO        self.assertRaises(TypeError, FormatString('{0[{1}]}').format, 'abcdefg', 4)

        # exceed maximum recursion depth
        self.assertRaises(ValueError, FormatString("{0:{1:{2}}}").format, 'abc', 's', '')
        self.assertRaises(ValueError, FormatString("{0:{1:{2:{3:{4:{5:{6}}}}}}}").format,
                          0, 1, 2, 3, 4, 5, 6, 7)

        # string format spec errors
# TODO        self.assertRaises(ValueError, FormatString("{0:-s}").format, '')
# TODO        self.assertRaises(ValueError, FormatString().format, "", "-")
# TODO        self.assertRaises(ValueError, FormatString("{0:=s}").format, '')

    def test_format_auto_numbering(self):
        class C:
            def __init__(self, x=100):
                self._x = x
            def __format__(self, spec):
                return spec

        self.assertEqual(FormatString('{}').format(10), '10')
        self.assertEqual(FormatString('{:5}').format('s'), 's    ')
        self.assertEqual(FormatString('{!r}').format('s'), "'s'")
        self.assertEqual(FormatString('{._x}').format(C(10)), '10')
        self.assertEqual(FormatString('{[1]}').format([1, 2]), '2')
        self.assertEqual(FormatString('{[a]}').format({'a':4, 'b':2}), '4')
        self.assertEqual(FormatString('a{}b{}c').format(0, 1), 'a0b1c')
        self.assertEqual(FormatString('a{:{}}b').format('x', '^10'), 'a    x     b')
        self.assertEqual(FormatString('a{:{}x}b').format(20, '#'), 'a0x14b')

        # can't mix and match numbering and auto-numbering
        self.assertRaises(ValueError, FormatString('{}{1}').format, 1, 2)
        self.assertRaises(ValueError, FormatString('{1}{}').format, 1, 2)
        self.assertRaises(ValueError, FormatString('{:{1}}').format, 1, 2)
        self.assertRaises(ValueError, FormatString('{0:{}}').format, 1, 2)

        # can mix and match auto-numbering and named
        self.assertEqual(FormatString('{f}{}').format(4, f='test'), 'test4')
        self.assertEqual(FormatString('{}{f}').format(4, f='test'), '4test')
        self.assertEqual(FormatString('{:{f}}{g}{}').format(1, 3, g='g', f=2), ' 1g3')
        self.assertEqual(FormatString('{f:{}}{}{g}').format(2, 4, f=1, g='g'), ' 14g')
'''
    def test_buffer_is_readonly(self):
        self.assertRaises(TypeError, sys.stdin.readinto, b"")

    def test_encode_and_decode_kwargs(self):
        self.assertEqual('abcde'.encode('ascii', 'replace'),
                         'abcde'.encode('ascii', errors='replace'))
        self.assertEqual('abcde'.encode('ascii', 'ignore'),
                         'abcde'.encode(encoding='ascii', errors='ignore'))
        self.assertEqual('Andr\202 x'.decode('ascii', 'ignore'),
                         'Andr\202 x'.decode('ascii', errors='ignore'))
        self.assertEqual('Andr\202 x'.decode('ascii', 'replace'),
                         'Andr\202 x'.decode(encoding='ascii', errors='replace'))
'''
if __name__ == "__main__":
    unittest.main()
